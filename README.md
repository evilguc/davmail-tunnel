# DavMail Tunnel &middot;  [![License: MIT](https://img.shields.io/badge/license-MIT-blue.svg)](./LICENSE)
> DavMail packed into Docker container for using against corporate Webmails with MS Exchange

For paranoics who don't want to use Exchange on their Android (due to requesting 'remote wipe' administrative permission for instance).
The solution is to use [DavMail](http://davmail.sourceforge.net/) to build gateway via Exhange OWA or EWS endpoints.

## License

[MIT licensed](./LICENSE)
